/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Test;

/**
 *
 * @author Tier3
 */
public interface ITest {
    public List<Test> getAll();
    public Test getTest(String Id);
    public boolean Add(Test test);
    public boolean Update(Test test);
    public boolean Delete(Test test);        
}
