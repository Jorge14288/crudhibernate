/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import dao.TestDao;
import java.math.BigDecimal;
import java.util.List;
import model.Test;

/**
 *
 * @author Tier3
 */
public class CrudHibernate2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Creat();
       // ConsultarTodo();
      // ConsultarId();
      //Modificar();
      Eliminar();
    }
    public static void Crear(){
         Test test = new Test();        
        System.out.println("TEST ---------");
        BigDecimal dpi = new BigDecimal("121212");
        test.setDpi(dpi);
        test.setNombre("Robert");
        test.setTelefono("78978456");  
        test.setId( new BigDecimal("2"));
        TestDao dao;
        dao = new TestDao();
        dao.Add(test);
        System.out.println("\n END ---------"); 
    }
   public static void  Modificar(){
         Test test = new Test();        
        System.out.println("TEST ---------");
        BigDecimal dpi = new BigDecimal("777777");
        test.setDpi(dpi);
        test.setNombre("Rob");
        test.setTelefono("888888");  
        test.setId( new BigDecimal("2"));
        TestDao dao;
        dao = new TestDao();
        dao.Update(test);
        System.out.println("\n END ---------"); 
    }
    public static void  Eliminar(){
         Test test = new Test();        
        System.out.println("TEST ---------"); 
        test.setId( new BigDecimal("1"));
        TestDao dao;
        dao = new TestDao();
        dao.Delete(test);
        System.out.println("\n END ---------"); 
    }
   public static void  ConsultarTodo(){
        List<Test> lista;
        System.out.println("TEST ---------");
        TestDao dao;
        dao = new TestDao();
        lista = dao.getAll();
        for(Test t: lista){
            System.out.println("\t"+ t.getNombre());
        }
        System.out.println("\n END ---------"); 
    }
    public static void  ConsultarId(){         
        System.out.println("TEST ---------");
        TestDao dao;
        dao = new TestDao();
        System.out.println(dao.getTest("1").getNombre());
        System.out.println("\n END ---------"); 
    }
    
}
