/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Test;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author Tier3
 */
public class TestDao implements ITest {
Session session = HibernateUtil.getSession();
    @Override
    public List<Test> getAll() {
    Query query = session.createQuery("from Test");
    return query.list();
    }

    @Override
    public Test getTest(String Id) {
      try{
          Test test = new Test();
          Query query = session.createQuery("from Test where id = "+ Id +"");
          test = (Test)query.uniqueResult();
          return test;
        }catch(Exception ex){
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean Add(Test test) {
        try{
             session.beginTransaction();
             session.save(test);
             session.getTransaction().commit(); 
             return true;
        }catch(Exception ex){
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean Update(Test test) {
        try{
             session.beginTransaction();
             session.update(test);
             session.getTransaction().commit(); 
             return true;
        }catch(Exception ex){
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean Delete(Test test) {
        try{
             session.beginTransaction();
             session.delete(test);
             session.getTransaction().commit(); 
             return true;
        }catch(Exception ex){
            System.out.print(ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }
}
